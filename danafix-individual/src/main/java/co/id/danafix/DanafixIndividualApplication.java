package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanafixIndividualApplication {

    public static void main(String[] args) {
        SpringApplication.run(DanafixIndividualApplication.class, args);
    }
}
