package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DanafixConfigurationApplication {
    public static void main(String[] args) {
        SpringApplication.run(DanafixConfigurationApplication.class, args);
    }
}
